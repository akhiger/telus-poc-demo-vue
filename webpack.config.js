var ExtractTextPlugin = require('extract-text-webpack-plugin');
var extractTelusSCSS = new ExtractTextPlugin('css/telus.css');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');


module.exports = {

  entry:  {
    main : __dirname + "/app/main",
    solutionbuilder : __dirname + "/app/solution-builder"

  },
  output: {
    path: __dirname + "/public",
    filename: "[name].js"
  },

  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['es2015']
      }
    }, {
      test: /\.html$/,
      loader: 'raw'
    },

      { test: /\.scss$/,loader: ExtractTextPlugin.extract("style", "css!sass") }

    ]
  },
  plugins: [
    new ExtractTextPlugin('css/style.css', {
      allChunks: true
    }),

    new OptimizeCssAssetsPlugin({
      assetNameRegExp: /\.css$/g,
      cssProcessor: require('cssnano'),
      cssProcessorOptions: { discardComments: {removeAll: true } },
      canPrint: true
    })


  ]
}
