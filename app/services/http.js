function get(endPointUrl, resolve, reject) {

    console.log("in module get",endPointUrl )
    var xhr = new XMLHttpRequest()
    // var self = this
    xhr.open('GET', endPointUrl, true)


  xhr.onload = function () {
    console.log("--xhr", xhr);
    if (this.readyState == 4 && this.status == 200) {
      resolve(JSON.parse(xhr.response));
    } else {
     reject({
        status: this.status,
        statusText: xhr.statusText
      });
    }
  };
  xhr.onerror = function (error) {
    reject(error);
  };



  xhr.send()

}





module.exports = {
  get: get

};





/*

 export default {
 route: {
 waitForData: true, // wait until data is loaded

 data (transition) {
 authService.getUser((response) => {
 transition.next({ authData: response }) // set data
 })
 }
 },

 data () {
 return {
 authData: null
 }
 }
 }*/
