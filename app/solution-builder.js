require('./assets/scss/telus.scss');
require('./assets/scss/pages/solution-builder.scss');
import Vue from 'vue';
Vue.config.debug = true;

import HeaderComponent from './components/header/header';
import FooterComponent from './components/footer/footer';
import NavComponent from './components/nav/nav';
import ReportComponent from './components/reports/report';
import ModalComponent from './components/modal/modal';
import StepTrackerComponent from './components/step-tracker/step-tracker';

import VueResource from 'vue-resource'

 window.vm = new Vue({
  el: '#naas-portal',
  components: {
    'header-component': HeaderComponent,
    'nav-component': NavComponent,
    'footer-component': FooterComponent,
    'report-component': ReportComponent,
    'modal-component': ModalComponent,
    'steptracker-component': StepTrackerComponent
  },
  data: {
  },
  created() {
    this.log("Created!!!")
  },
  methods: {
    log: function(str){
      console.log(str)
    },
    broadcastEvent: function(e, str){
      this.$broadcast(e, str);
    }

  }
})


//vm.broadcastEvent('toggleModal', "<strong>kkk</strong>")




