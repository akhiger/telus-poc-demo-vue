import Vue from 'vue';
Vue.config.debug = true;

import HeaderComponent from './components/header/header';
import NavComponent from './components/nav/nav';
import FooterComponent from './components/footer/footer';


new Vue({
  el: '#naas-portal',
  components: {
    'header-component': HeaderComponent,
    'nav-component': NavComponent,
    'footer-component': FooterComponent
  },
  data: {
  }
});



