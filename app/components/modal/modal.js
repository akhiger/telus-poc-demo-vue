//require('../../assets/scss/telus.scss');
//require('./modal.scss');

import Vue from 'vue';
import template from './modal.html';



const ModalComponent = Vue.extend({
  template,
  data() {
    return {
      isShown: false,
      modalContent: "<i>italic</i>"
    }
  },
  created() {
    //this.toggleMenu();
    var self = this;
    this.$on("toggleModal",  function (content) {
      self.toggleModal(content)
    })
  },
  methods: {
    toggleModal: function (content) {
      this.isShown = !this.isShown;
      this.modalContent = content;
      //alert(this.showSecondaryNav);
    }
  }
})

export default ModalComponent;









/*

// Get the modal
  var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function () {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}*/
