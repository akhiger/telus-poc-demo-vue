/*require('../../assets/scss/telus.scss');
require('./nav.scss');*/

import Vue from 'vue';
import template from './nav.html';



const NavComponent = Vue.extend({
  template,
  data() {
    return {
      navMenuObj: [{
        label: "Account Hub", img: "fa-chevron-left", isSelected: true, link: "#", isSection: true
      },
      {
        label: "Locations", img: "fa-globe", isSelected: false, link: "#"
      },
      {
        label: "Reports", img: "fa-bar-chart", isSelected: false, link: "#"
      },
      {
        label: "Orders", img: "fa-truck", isSelected: false, link: "#"
      },
      {
        label: "User Mgmt", img: "fa-user-plus", isSelected: false, link: "#"
      },
      {
        label: "Configuration", img: "fa-gear", isSelected: false, link: "#"
      }
      ],

      selectedMenu:{
        label: "Account Hub", img: "fa-chevron-left", link: "#", isSection: true
      },
      nowSelecting: false


    }
  },
  created() {
    //this.pusher = new Pusher('9fd1b33fcb36d968145f');
  },
  methods: {
    toggleMenu: function(){
      this.nowSelecting = !this.nowSelecting;
    },
    selectMenu: function(menu){
      this.selectedMenu.isSelected =false;
      menu.isSelected = true;
      this.selectedMenu = menu;
      this.nowSelecting = !this.nowSelecting;
    }
    /*newSubscription() {
      this.channels.push({
        term: this.newSearchTerm,
        active: true
      });
      this.newSearchTerm = '';
    },
    toggleSearch(channel) {
      for (let ch of this.channels) {
        if (ch.term === channel.term) {
          ch.active = !ch.active;
          break;
        }
      }
    },
    clearSearch(channel) {
      this.channels = this.channels.filter((ch) => {
        if (ch.term === channel.term) {
          ch.active = false;
        }
        return ch.term !== channel.term;
      });
    }*/
  }
});

export default NavComponent;





