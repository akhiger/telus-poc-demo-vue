//require('../../assets/scss/telus.scss');
//require('./header.scss');

import Vue from 'vue';

import template from './header.html';
/*import SubscriptionComponent from '../subscription-component/subscription-component';*/



const HeaderComponent = Vue.extend({
  template,
  components: {
    /*'subscription-component': SubscriptionComponent*/

  },
  data() {
    return {
      showSecondaryNav: false

    }
  },
  created() {
    this.toggleMenu();
   /* this.pusher = new Pusher('9fd1b33fcb36d968145f');*/
  },
  methods: {
    toggleMenu: function() {
      this.showSecondaryNav = !this.showSecondaryNav;
      //alert(this.showSecondaryNav);
    }

    /*toggleMenu(elId){

      var flag =  document.getElementById(this.dataset.target).style.display;

      document.getElementById(elId).style.display = (flag == 'none') ? 'block' : 'none'

    }*/

 /*   newSubscription() {
      this.channels.push({
        term: this.newSearchTerm,
        active: true
      });
      this.newSearchTerm = '';
    },
    toggleSearch(channel) {
      for (let ch of this.channels) {
        if (ch.term === channel.term) {
          ch.active = !ch.active;
          break;
        }
      }
    },
    clearSearch(channel) {
      this.channels = this.channels.filter((ch) => {
        if (ch.term === channel.term) {
          ch.active = false;
        }
        return ch.term !== channel.term;
      });
    }*/
  }
});

export default HeaderComponent;





